package com.brr.hrms.emps;

import com.brr.hrms.BasicEmployee;

public class ITEmployee extends BasicEmployee {
	//additional data
private	String project;
private	String team;
private	String dead_line;
	
	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getTeam() {
		return team;
	}

	public void setTeam(String team) {
		this.team = team;
	}

	public String getDead_line() {
		return dead_line;
	}

	public void setDead_line(String dead_line) {
		this.dead_line = dead_line;
	}

	public ITEmployee(int id, String project) {
		super(id);
		this.project = project;
	}

	boolean iscomplete() {
		boolean b=false;
		return b;
		
	}

}
