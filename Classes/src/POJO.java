import java.util.ArrayList;

public class POJO {
	private int id;
	private String name;
	private String loc;
	private float sal;

	public POJO(int id, String name, String loc, float sal) {
		super();
		this.id = id;
		this.name = name;
		this.loc = loc;
		this.sal = sal;
	}
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the loc
	 */
	public String getLoc() {
		return loc;
	}
	/**
	 * @param loc the loc to set
	 */
	public void setLoc(String loc) {
		this.loc = loc;
	}
	/**
	 * @return the sal
	 */
	public float getSal() {
		return sal;
	}
	/**
	 * @param sal the sal to set
	 */
	public void setSal(float sal) {
		this.sal = sal;
	}
	public static void main(String args[]) {
		POJO p1 = new POJO(101,"sindhu","hyd",10000.0f);
		POJO p2 = new POJO(102,"mounika","hyd",10000.0f);
		POJO p3 = new POJO(103,"priya","hyd",10000.0f);
		POJO p4 = new POJO(104,"kai","hyd",10000.0f);
		
		p2.setSal(1155.0f);
		//immigration
		p3.setLoc("india");
		
		ArrayList emp = new ArrayList();
		emp.add(p1);
		emp.add(p2);
		emp.add(p3);
		emp.add(p4);
		
		print(emp);
	}
	
	private static void print(ArrayList emp) {
		// TODO Auto-generated method stub
		
		System.out.println("id  name loc sal ");
		for(Object o:emp) {
			POJO temp =(POJO)o;
			System.out.println(" "+temp.getId()+" "+temp.getName()+" "+temp.getLoc()+" "+temp.getSal());
					
					
		}
	
	
	}
}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
		


